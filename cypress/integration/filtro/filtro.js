import { Given, Then, When } from "cypress-cucumber-preprocessor/steps";
import PageObjects from '../../support/PageObjects/PageObjects'

const pageObjects = new PageObjects();

Given('Que el usuario esta en la pagina de laptop', () => {

    cy.visit('/index.html')
    pageObjects.selectLaptop();

})
