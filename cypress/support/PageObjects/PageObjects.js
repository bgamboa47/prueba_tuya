

class PageObjects {

    getItemLaptops() {
        cy.get('a[id="itemc"]:contains(Laptops)').as('laptops')
        cy.get('@laptops').should('exist')

        return cy.get('@laptops');
    }

    clickItemLaptops() {
        cy.get('a[id="itemc"]:contains(Laptops)').click().as('itemLaptop')

        return cy.get('@itemLaptop');
    }

    selectLaptop() {
        this.getItemLaptops();
        this.clickItemLaptops();
    }



}

export default PageObjects;